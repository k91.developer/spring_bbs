package kr.ac.kopo.ctc.spring.board.repository;

import javax.transaction.Transactional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GongjiRepositoryTest {

	@Autowired	// GongjiRepository DI
	private GongjiRepository gongjiRepository;
	
	@Autowired // UserRepository DI
	private UserRepository userRepository;
	
	@Ignore
	@Test
	public void userck() {	// Name으로 검색 메소드 테스트
		User u = userRepository.findByName("이상훈");
		if (u == null) {	// 해당 name이 없어서 u값이 null일 경우
			System.out.println("null");
			u = new User("이상훈");	// 새로운 객체 생성 (user_id부여)
			userRepository.save(u);	// 객체 저장
		} else {
			System.out.println(u.getName());
		}
	}
		
	@Ignore
	@Test
	//@Transactional  // 오류 발생시 롤백 실행
	public void oneToMany_OneWay() {
				
		User u = new User("강민규");
		System.out.println("id" + u);
		
	// 미리 아이템 100개를 적재(임의의 데이터 생성)
		for (int i = 0; i < 100; i++) {
			Gongji be = new Gongji();
			be.setContent(dummyString()); // dummy를 content 값으로
			if (i % 3 == 0) {
				be.setAuthor("Jung"); // 작성자를 Jung으로
			} else if (i % 3 == 1) {
				be.setAuthor("second"); // 작성자를 Second으로
			} else {
				be.setAuthor("third"); // 작성자를 third으로
			}
			be.setUser(u);	// setUser 생성
			gongjiRepository.save(be);	// 저장
		}
		


		// 한 페이지 아이템 10개, 0번째 페이지 호출
		Page<Gongji> page = gongjiRepository.findAll(PageRequest.of(0, 10));
		printPageData("simple", page);

		// 한 페이지 아이템 10개, 글번호 내림차순으로, 0번째 페이지 호출
		page = gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(0, 10));
		printPageData("sort_seq_desc", page);

		// 한 페이지에 아이템 10개, 글번호 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAll(PageRequest.of(2, 10, new Sort(Direction.DESC, "id")));
		printPageData("sort", page);

		// 한페이지에 아이템 10개, 글작성자 "second", 0번째 페이지 호출
		page = gongjiRepository.findAllByAuthor("second", PageRequest.of(0, 10));
		printPageData("sort_author", page);

		// 한페이지 아이템 10개, 작성자 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAll(PageRequest.of(2, 10, new Sort(Direction.DESC, "author")));
		printPageData("sort_author_desc", page);

		// 한페이지 아이템 10개, 검색어 중, 글번호 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAllSearchContent("bc", PageRequest.of(2, 10, new Sort(Direction.DESC, "id")));
		printPageData("sort_search_desc", page);
		
		// 한페이지 아이템 10개, 검색어 중, 글번호 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAllSearchTitle("목", PageRequest.of(0, 5, new Sort(Direction.DESC, "id")));
		printPageData("sort_search_desc", page);
	}

	// 페이지 데이터 출력
	private void printPageData(String label, Page<Gongji> page) {
		if (page == null || page.getSize() <= 0)
			return;

		for (int i = 0; i < page.getSize(); i++) {
			Gongji be = page.getContent().get(i);
			System.out.println("[" + label + "] " + be.getId() + " " + be.getTitle() + " " + be.getContent());
		}
	}

	// 더미 문자열 반환 // 임의의 content
	private String dummyString() {

		String[] dummy = { "abc", "bcd", "cde", "def" };
		int rand = (int) (System.currentTimeMillis() % dummy.length);
		return dummy[rand];
	}
	
	@Ignore
	@Test	// update메소드 테스트
	@Transactional	// 도중에 오류가 났을 경우 rollback을 하기 위한 트랜잭션 어노테이션
	public void update() {
		
		int ret = gongjiRepository.update("333", "333", 2142);
		System.out.println(ret);
		
	}
	
	
}
