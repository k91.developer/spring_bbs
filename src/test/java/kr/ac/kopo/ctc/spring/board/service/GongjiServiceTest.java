package kr.ac.kopo.ctc.spring.board.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import kr.ac.kopo.ctc.spring.board.VO.GongjiVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GongjiServiceTest {
	
	@Autowired
	GongjiService gongjiService;
	
	@Test
	public void userCk() {
		GongjiVO vo = gongjiService.gongjiVO(1, true, "title", "강민규");
		
		
		assertEquals(1, vo.getTotalpage());
		
		
	}
}
