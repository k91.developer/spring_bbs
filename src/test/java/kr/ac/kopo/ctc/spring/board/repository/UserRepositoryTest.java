package kr.ac.kopo.ctc.spring.board.repository;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
   
   @Autowired
   private UserRepository userRepository;
   
	@Before	// 테스트 전에 출력
	public void before() {
		System.out.println("before");
	}
	
	@After // 테스트 후에 출력
	public void after() {
		System.out.println("after");
	}
	
   @Ignore	// 테스트 무시
   @Test	// 테스트 실행
   @Transactional   // 도중에 오류가 났을 경우 rollback을 하기 위한 트랜잭션 어노테이션
   public void oneToMany_TwoWay() {
      //디버깅(MEMBER 35번)
//      User u = userRepository.findById(55).get();
//      
//      System.out.println(m.getName());
      
      User first = new User("Jung");
      first.addGongji(new Gongji("제목1", "1234", new Date(), "Jung", 0 ));
      first.addGongji(new Gongji("제목2", "1234", new Date(), "Jung", 0 ));
      
      User second = new User("Dong");
      second.addGongji(new Gongji("제목3", "1234", new Date(), "Dong", 0 ));
      
      User third = new User("Min");
      
      userRepository.save(first); // Member를 저장, phone why???
      userRepository.save(second);
      userRepository.save(third);
      
      List<User> list = userRepository.findAll();
      
      for(User u : list) {
         System.out.println(u.toString());
         
      }
      userRepository.deleteAll();
   }
   
   
   @Test	// Name으로 검색하기 위한 메소드 테스트
   public void test() {
	   String u =userRepository.findByName("나오").getName();
	  System.out.println(u);
   }
   
   	@Ignore
	@Test	// userid 존재여부 확인하기 위한 메소드 테스트
	@Transactional	// 도중에 오류가 났을 경우 rollback을 하기 위한 트랜잭션 어노테이션
	public void count() {
		
		int ret = userRepository.count("k910430");
		System.out.println(ret);
	}
   	
   	@Ignore
   	@Test	// 해당 이름, 아이디 존재 확인 위한 메소드	위한 테스트
   	public void find() {
   		String result2 = userRepository.findByName("강민규").getUserid();
   		String result = userRepository.findByUserid("k910430").getName();
   		System.out.println(result2);
   		System.out.println(result);
   	}
   	@Ignore
   	@Test	// 로그인 체크 메소드 위한 테스트
   	public void loginCK() {
   		int ret = userRepository.loginCK("sera", " ");
   		System.out.println("loginCK : " + ret);
   	}
}