package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import kr.ac.kopo.ctc.spring.board.domain.Member;
import kr.ac.kopo.ctc.spring.board.domain.Phone;






@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberRepositoryTest {
	
	@Autowired
	private MemberRepository memberRepository;
	
	@Test
	@Transactional	// 도중에 오류가 났을 경우 rollback을 하기 위한 트랜잭션 어노테이션
	public void oneToMany_TwoWay() {
		//디버깅(MEMBER 35번)
		Member m = memberRepository.findById(55).get();
		
		System.out.println(m.getName());
		
//		Member first = new Member("Jung");
//		first.addPhone(new Phone("010-1111-1111"));
//		first.addPhone(new Phone("011-1111-1111"));
//		
//		Member second = new Member("Dong");
//		second.addPhone(new Phone("010-2222-2222"));
//		
//		Member third = new Member("Min");
//		
//		memberRepository.save(first); // Member를 저장, phone why???
//		memberRepository.save(second);
//		memberRepository.save(third);
//		
//		List<Member> list = memberRepository.findAll();
//		
//		for(Member m : list) {
//			System.out.println(m.toString());
//			
//		}
//		memberRepository.deleteAll();
	}

}
