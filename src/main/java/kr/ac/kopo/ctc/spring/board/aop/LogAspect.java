package kr.ac.kopo.ctc.spring.board.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
	
	private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);
	///// 교재 135~136페이지
	// 메서드 실행 전
	@Before("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onBeforeHandler(JoinPoint joinpoint) {
		logger.info("========== onBeforeThing");
	}
	// 메서드가 호출된 후에(정상종료, 오류 상관없이)
	@After("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onAfterHandler(JoinPoint joinpoint) {
		logger.info("========== onAfterThing");
	}
	// 메서드를 실행했는데 정상종료 되었을 경우
	@AfterReturning(pointcut = "execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))", returning = "str")
	public void onAfterReturningHandler(JoinPoint joinpoint, Object str) {
		logger.info("@AfterReturning:" + str);
		logger.info("========== onAfterReturningHandler");
	}
	// 실행 전후
	@Around("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public Object onAround(ProceedingJoinPoint proceedingJoinPoint) {
		logger.info("@Around start");
		Object result = null;
		try {
			result = proceedingJoinPoint.proceed();
		}catch(Throwable e) {
			e.printStackTrace();
		}
		logger.info("@Around End");
		return result;
	}
	// 예외 발생시
	@AfterReturning(pointcut = "onPointcut()")
	public void onAfterReturningByPointcut() {
		logger.info("========== onAfterReturningByPointcut");
	}
	@Pointcut("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onPointcut() {}	
}
