package kr.ac.kopo.ctc.spring.board.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import kr.ac.kopo.ctc.spring.board.VO.GongjiVO;
import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;
import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.repository.UserRepository;

@Service
public class GongjiServiceImpl implements GongjiService{
	
	private static final Logger logger = LoggerFactory.getLogger(GongjiServiceImpl.class);
	
	@Autowired
	private GongjiRepository gongjiRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override	// 페이징
	public List<Gongji> findAll(int page) {
		return gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(page, 10)).getContent();
	}
	@Override	// 작성자 검색
	public List<Gongji> findAllSearchAutohr(String author, int page){
		return gongjiRepository.findAllByAuthor(author, PageRequest.of(page, 10)).getContent();
	}
	@Override	// 해당 아이디 검색
	public Gongji findByOne(int id) {
		
		return gongjiRepository.findById(id).get();
	}
	@Override	// 해당 아이디 데이터 삭제(delete)
	public void delete(int id) {
		gongjiRepository.deleteById(id);
	}
	@Override	// 데이터 생성 (create)
	public void create(String title, String content, String author) {
		Gongji g = new Gongji();
		User u = userRepository.findByUserid(author);
//		if (u == null) {		
//			u = new User(author);
//			userRepository.save(u);
//		}
		g.setUser(u);
		g.setTitle(title);
		g.setContent(content);
		g.setDate(new Date());
		g.setViewcnt(0);
		g.setAuthor(author);
		gongjiRepository.save(g);
		
	}
	@Override	// 제목 검색
	public List<Gongji> findAllSerachTitle(String title, int page) {
		return gongjiRepository.findAllSearchTitle(title, PageRequest.of(page, 10)).getContent();
	}
	@Override	// 내용 검색
	public List<Gongji> findAllSerachContent(String content, int page) {
		return gongjiRepository.findAllSearchContent(content, PageRequest.of(page, 10)).getContent();
	}
	@Override	// 전체 데이터 수 확인
	public int findCount() {
		return (int) gongjiRepository.count();
	}
	@Override	// 데이터 수정 (update)
	public Gongji update(String title, String content, int id) {
		
		Gongji g = gongjiRepository.findById(id).get();
		g.setTitle(title);
		g.setDate(new Date());
		g.setContent(content);
		return gongjiRepository.save(g);
	}
	@Override	// 조회수 변경
	public Gongji view(int id) {
		Gongji g = gongjiRepository.findById(id).get();
		g.setViewcnt(g.getViewcnt()+1);
		return gongjiRepository.save(g);
	}
	// 작성자 검색한 데이터들의 총페이지
	public int CountSearchAuthor(String author) {
		return gongjiRepository.findAllByAuthor(author, PageRequest.of(0, 10)).getTotalPages();	
	}
	// 제목 검색한 데이터들의 총페이지
	public int CountSearchTitle(String title) {
		return gongjiRepository.findAllSearchTitle(title, PageRequest.of(0, 10)).getTotalPages();	
	}
	@Override	// 새로운 회원가입자 
	public void newUser(String userid, String passwd, String user, String email) {
		User u = new User();
		u.setUserid(userid);
		u.setName(user);
		u.setEmail(email);
		u.setPassword(passwd);
		userRepository.save(u);
	}
	@Override	// 아이디 중목체크
	public int IdCk(String Id) {
		return userRepository.count(Id);
	}
	@Override	// 로그인 체크
	public int loginCK(String Id, String passwd) {
		return userRepository.loginCK(Id, passwd);
	}
	@Override	// 해당아이디의 실명
	public String findById(String id) {
		return userRepository.findByUserid(id).getName();	
	}
	@Override	// 페이징
	public GongjiVO gongjiVO(int page, boolean searching, String opt, String value) {
		int totalpage = 0;
		if(searching == false) { // 미검색
			totalpage = gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(0, 10)).getTotalPages();
		}else if(opt.equals("author")) { // 작성자 검색
			totalpage = gongjiRepository.findAllByAuthor(value, PageRequest.of(0, 10)).getTotalPages();
		}else if(opt.equals("title")) { // 제목 검색
			totalpage = gongjiRepository.findAllSearchTitle(value, PageRequest.of(0, 10)).getTotalPages();	
		} else {		
		}
		// 블럭 수
		int block = 10;
		// 블럭 시작
		int Sblock = (page-1)/block*block+1;
		//블럭끝
		int Eblock = Sblock + 9;
		if(Eblock>totalpage) Eblock = totalpage;
		// 이전
		int Before = Sblock - 10;
		if(Before<0) Before = 1;
		// 다음
		int Next=Sblock+10;
		if(Next>totalpage) Next = totalpage;
		
		GongjiVO p = new GongjiVO();
		p.setBefore(Before);
		p.setSblock(Sblock);
		p.setEblock(Eblock);
		p.setNext(Next);
		p.setTotalpage(totalpage);
		
		return p;
	}		
}
