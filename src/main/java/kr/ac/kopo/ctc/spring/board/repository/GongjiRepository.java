package kr.ac.kopo.ctc.spring.board.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;

@Repository
public interface GongjiRepository extends JpaRepository<Gongji, Integer>{
	// 전체 보기
	Page<Gongji> findAll(Pageable pageable);
	
	//전체 보기 (ID내림차순)
	Page<Gongji> findAllByOrderByIdDesc(Pageable pageable);
	
	// 작성자 검색
	Page<Gongji> findAllByAuthor(String author, Pageable pageable);
	
	// 제목검색
	@Query("select t from Gongji t where title like concat ('%', :searchString, '%')")
	Page<Gongji> findAllSearchTitle(@Param("searchString") String searchString, Pageable pageable);
	
	// 내용검색
	@Query("select t from Gongji t where content like concat ('%', :searchString, '%')")
	Page<Gongji> findAllSearchContent(@Param("searchString") String searchString, Pageable pageable);
		
	// 수정
	@Modifying
	@Query("update Gongji t set t.title=:title, t.content=:content where t.id=:id")
	int update(@Param("title") String title, @Param("content") String content, @Param("id") int id);
		
	
}
