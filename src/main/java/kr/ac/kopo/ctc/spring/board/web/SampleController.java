package kr.ac.kopo.ctc.spring.board.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.kopo.ctc.spring.board.repository.SampleRepository;
import kr.ac.kopo.ctc.spring.board.service.SampleService;

@Controller // 해당 클래스가 Controller임을 나타내기 위한 어노테이션
public class SampleController {
	
	private static final Logger logger = LoggerFactory.getLogger(SampleController.class);
	
	@Autowired
	private SampleRepository sampleRepository;
	
	@Autowired
	private SampleService sampleService;
	
	@RequestMapping(value = "/sample/noAop")
	@ResponseBody
	public String noAop() {
		return sampleService.testNoAop();
	}
	
	@RequestMapping(value="/sample/aop")
	@ResponseBody
	public String aop() {
		return sampleService.testAop();
	}
	
	@RequestMapping(value="/sample/noTransactional")
	@ResponseBody
	public String noTransactonal() {
		return sampleService.testNoTransactional();
	}
	
	@RequestMapping(value = "/sample/transactional")
	@ResponseBody
	public String transactonal() {
		return sampleService.testTransactional();
	}
}