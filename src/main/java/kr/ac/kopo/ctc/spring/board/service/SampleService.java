package kr.ac.kopo.ctc.spring.board.service;

public interface SampleService {
	// AOP
	String testNoAop();
	String testAop();
	
	// Transactional
	String testNoTransactional();
	String testTransactional();
}
