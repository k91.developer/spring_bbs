package kr.ac.kopo.ctc.spring.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
  
  // Name으로 검색
  User findByName(String name);
  
  // UserId로 검색
  User findByUserid(String userid);
  
  // @Modifying
  // userid가 존재하는지 유무 판단하기 위한 메소드(아이디 중복체크용)
  @Query("select count(u) from User u where u.userid=:userid")
  int count(@Param("userid") String userid);
  
  //로그인체크를 위한 메소드
  @Query("select count(u) from User u where u.userid=:userid and u.password=:password")
  int loginCK(@Param("userid") String userid, @Param("password") String password );

}