package kr.ac.kopo.ctc.spring.board.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.ac.kopo.ctc.spring.board.domain.Sample;

@Controller
public class HelloController { // model과 view를 연결
	
	@RequestMapping(value="/hello") // 주소상의 hello : input URL // controller의 역할 2가지.
	public String hellSpringBoot(Model model) {	// model 세팅
		model.addAttribute("name","강민규");
		model.addAttribute("age","29");
		
		
		Sample s = new Sample();
		s.setId(5L);
		s.setTitle("1234");
		
		model.addAttribute("sample",s);
		
		return "hello"; //hello.jsp로 이동 // view 지엉
	}
}