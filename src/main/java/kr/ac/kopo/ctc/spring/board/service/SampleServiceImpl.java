package kr.ac.kopo.ctc.spring.board.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.ac.kopo.ctc.spring.board.domain.Sample;
import kr.ac.kopo.ctc.spring.board.repository.SampleRepository;

@Service
public class SampleServiceImpl implements SampleService{
	
	private static final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);
	
	@Autowired
	private SampleRepository sampleRepository;
	
	@Override
	public String testNoAop() {
		String msg = "Hello, Spring Boot NO AOP";
		logger.info(msg);
		return msg;
	}
	
	@Override
	public String testAop() {
		String msg = "Hello, Spring Boot AOP";
		logger.info(msg);
		return msg;
	}
	
	@Override
	public String testNoTransactional() {
		Sample sample = sampleRepository.findById(2L).get();
		sample.setTitle("update1");
		sampleRepository.save(sample);
		// 에러발생
		throw new RuntimeException("Spring Boot No Transactional test");
	}
	
	@Override
	@Transactional // 트랜젝션 어노테이션
	public String testTransactional() {
		Sample sample = sampleRepository.findById(2L).get();
		sample.setTitle("update1");
		sampleRepository.save(sample);
		// 에러 발생
		throw new RuntimeException("Spring Boot Transactional Test");
	}

}
