package kr.ac.kopo.ctc.spring.board.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sample {
	
	@Id // 기본 키를 직접 할당하기 위해서는 @Id 어노테이션만 사용
	@GeneratedValue // 원하는 키 생성 전략을 선택 (Oracle의 경우 Sequence가 기본, mysql의 경우 Identity가 기본)
	@Column // 필드를 컬럼에 매핑한다.
	private Long id;
	
	@Column
	private String title;
	
	@Column
	private String level;
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
