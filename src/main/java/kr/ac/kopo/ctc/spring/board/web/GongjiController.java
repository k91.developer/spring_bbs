package kr.ac.kopo.ctc.spring.board.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.service.GongjiService;
import kr.ac.kopo.ctc.spring.board.service.GongjiServiceImpl;

@Controller
public class GongjiController {
		
	@Autowired
	private GongjiService gongjiService;
	
	// 페이징
	@RequestMapping(value="/gongji/check")
	public String findAll(Model model, @RequestParam HashMap<String, String> map) { // (모델 ,파라미터)
		// 파라미터 설정
		int page=0;
		String pages = map.get("page");
		if(pages==null) {
			page = 1;
		}else {
			page = Integer.parseInt(pages);
		}
		// 파라미터 값 확인
		System.out.println(page);
		// 총 데이터 값
		int total = gongjiService.findCount();
		System.out.println("total Data :" + total);
		// 페이지당 출력수
		int cnt = 10;
		// 총페이지 수
		int totalpage = total/cnt;
		if(total%cnt!=0) {
			totalpage+=1;
		}
		System.out.println("total Page :" + totalpage);
		// 블럭 수
		int block = 10;
		// 블럭 시작
		int Sblock = (page-1)/block*block+1;
		//블럭끝
		int Eblock = Sblock + 9;
		if(Eblock>totalpage) Eblock = totalpage;
		// 이전
		int Before = Sblock - 10;
		if(Before<0) Before = 1;
		// 다음
		int Next=Sblock+10;
		if(Next>totalpage) Next = totalpage;
		
		List<Gongji>gongji_list = gongjiService.findAll(page-1); 
		// 모델설정
		model.addAttribute("gongji_list", gongji_list);
		model.addAttribute("totla_Data",total);
		model.addAttribute("pages",totalpage);
		model.addAttribute("block",block);
		model.addAttribute("page",page);
		model.addAttribute("Sblock",Sblock);
		model.addAttribute("Eblock",Eblock);
		model.addAttribute("Before",Before);
		model.addAttribute("Next",Next);
		return "gongji/check";
	}
	
	// 게시물 보기
	@RequestMapping (value="/gongji/findOne")
	public String findOne(Model model, @RequestParam HashMap<String, String> map) {
		String page = map.get("page");
		System.out.println(page);
		String ids=map.get("id");
		int id = Integer.parseInt(ids);
		System.out.println(id);
		gongjiService.view(id);
		Gongji gongji=gongjiService.findByOne(id);
		model.addAttribute("gongji", gongji);
		model.addAttribute("page",page);
		model.addAttribute("id",id);
		return "gongji/findOne";
	}
	// 글작성
	@RequestMapping(value= "/gongji/create") 
	public String create(Model model, @RequestParam HashMap<String, String> map) {
		Date date = new Date();
		model.addAttribute("date", date);
		return "gongji/create";
	}
	// 글수정 뷰어
	@RequestMapping(value= "/gongji/update") 
	public String update(Model model, @RequestParam HashMap<String, String> map) {
		String ids=map.get("id");
		int id = Integer.parseInt(ids);
		System.out.println(id);
		
		Gongji gongji=gongjiService.findByOne(id);
		model.addAttribute("gongji", gongji);
		model.addAttribute("id", id);
		return "gongji/update";
	}
	// 등록
	@RequestMapping(value= "/gongji/write")
	public String write(Model model, @RequestParam HashMap<String, String> map) {
			
		String title=map.get("title");
		String content=map.get("content");
		String author=map.get("author");
		String ids=map.get("id");
		if(ids==null) {
			int result = gongjiService.IdCk(author);
			if(result>0) {
				gongjiService.create(title, content, author);
			}else {
				model.addAttribute("msg","false");
			}	
		}else {
			int id = Integer.parseInt(ids);
			System.out.println(id);
			gongjiService.update(title, content, id);
		}
		return "gongji/write";
	}
	// 삭제
	@RequestMapping (value="/gongji/delete")
	public String delete(Model model, @RequestParam HashMap<String, String> map) {
		String ids=map.get("id");
		int id = Integer.parseInt(ids);
		System.out.println(id);
		gongjiService.delete(id);
		return "gongji/delete";
	}
	
	// 작성자 조회 & 제목 조회
	@RequestMapping(value= "/gongji/search") 
	public String findByAuthor(Model model, @RequestParam HashMap<String, String> map) {
		// 검색 관련 파라미터 설정
		String opt=map.get("opt");
		String search=map.get("search");
		if(search.equals("")){
			return "redirect:findAll";
		}
		model.addAttribute("opt", opt);
		model.addAttribute("search", search);
		List<Gongji> gongji_list = null;
		int totalpage = 0;
				
		// 파라미터 설정
		int page=0;
		String pages = map.get("page");
		if(pages==null) {
			page = 1;
		}else {
			page = Integer.parseInt(pages);
		}
		// 파라미터 값 확인
		System.out.println(page);
		// 총 데이터 값
		int total = gongjiService.findCount();
		System.out.println("total Data :" + total);
		// 페이지당 출력수
		int cnt = 10;
		System.out.println("total Page :" + totalpage);
		// 블럭 수
		int block = 10;
		// 블럭 시작
		int Sblock = (page-1)/block*10+1;
		//블럭끝
		int Eblock=Sblock + 9;
		// 이전
		int Before = Sblock - 10;
		if(Before<0) Before = 1;
		// 다음
		int Next=Sblock+10;
		if(opt.equals("author")) {
			totalpage = gongjiService.CountSearchAuthor(search);
			gongji_list = gongjiService.findAllSearchAutohr(search,  page-1);
		}else if(opt.equals("title")) {
			totalpage = gongjiService.CountSearchTitle(search);
			gongji_list = gongjiService.findAllSerachTitle(search, page-1);
		}
		if(Eblock>totalpage) Eblock = totalpage;
		if(Next>totalpage) Next = totalpage;
		// 모델설정
		model.addAttribute("gongji_list", gongji_list);
		model.addAttribute("pages",totalpage);
		model.addAttribute("page",page);
		model.addAttribute("Sblock",Sblock);
		model.addAttribute("Eblock",Eblock);
		model.addAttribute("Before",Before);
		model.addAttribute("Next",Next);	
		model.addAttribute("page",page);
		model.addAttribute("gongji_list", gongji_list);
		
		return "gongji/search";
	}
	
	// 회원가입
	@RequestMapping(value="/gongji/sign")
	public String sign(Model model, @RequestParam HashMap<String, String> map) {
	return "gongji/sign";
	}
	
	// 회원가입 OK
	@RequestMapping(value="/gongji/signOK")
	public String signOK(Model model, @RequestParam HashMap<String, String> map) {
		String id=map.get("id");
		String passwd=map.get("passwd");
		String username=map.get("username");
		String email=map.get("email");
		
		// 해당 아이디가 존재할 경우 result는 양수 없으면 음수
		int result = gongjiService.IdCk(id); 
		if (result>0) {
			model.addAttribute("msg", "false");
		}else {
				gongjiService.newUser(id, passwd, username, email);
				model.addAttribute("msg", "true");
		}
	
		return "gongji/signOK";
	}
	
//	// 회원가입 중복체크
//	@RequestMapping(value="/gongji/IdCK")
//	public String IdCk(Model model, @RequestParam HashMap<String, String> map) {
//		String id=map.get("id");
//		int result = gongjiService.CountSearchAuthor("id");
//		if (result>0) {
//				model.addAttribute("msg", "false");
//		}else {
//				model.addAttribute("msg", "true");
//		}
//		return "gongji/IdCK";
//	}
	
	// 로그인
	@RequestMapping(value="/gongji/login")
		public String login(Model model, @RequestParam HashMap<String, String> map) {
		return "gongji/login";
	}
		
	// 로그인 OK
	@RequestMapping(value="/gongji/loginOK")
	public String loginOK(Model model, @RequestParam HashMap<String, String> map, HttpSession session) {
		String id=map.get("id");
		String passwd=map.get("passwd");
		// 해당 id의 실명 조회
		String name = gongjiService.findById(id);
		// 해당 아이디와 비밀번호 체크 & 일치할 경우 로그인 성공
		int result = gongjiService.loginCK(id, passwd);
		if(result>0) {
			// 로그인 성공할 경우 세션 부여
			session.setAttribute("sessionid", id);
			session.setAttribute("sessionname", name);
			
			model.addAttribute("classname", this.getClass());
			model.addAttribute("msg", "true");
		}else {
			model.addAttribute("msg", "false");
		} 
		return "gongji/loginOK";
	}	
		
	// 로그아웃
	@RequestMapping(value="/gongji/logout")
	public String logout(Model model, @RequestParam HashMap<String, String> map, HttpSession session) {
		// 세션 일괄삭제
		session.invalidate();
		 
		return "gongji/logout";
	}	
	
	
	@RequestMapping(value= "/gongji/findAll")	// 전체보기 및 검색
	public String check(Model model, @RequestParam HashMap<String, String> map) {	
		// 파라미터 설정
		// 검색 관련 파라미터 설정
		String opt=map.get("opt");
		String search=map.get("search");
		int page=0;
		String pages = map.get("page");
		
		if(pages==null) {
			page = 1;
		}else {
			page = Integer.parseInt(pages);
		}
		// 파라미터 값 확인
		System.out.println("page : "+ page);
		System.out.println("opt ;" +  opt);
		System.out.println("search ;" + search);	
			
		boolean searching = false;		
		List<Gongji> gongji_list = null;
		if(search==null||search.equals("")){
			//검색안할때
			searching = false;
			gongji_list = gongjiService.findAll(page-1);
		}else {
			//검색할때
			searching = true;
			if(opt.equals("author")) {
				gongji_list = gongjiService.findAllSearchAutohr(search,  page-1);
			}else if(opt.equals("title")) {
				gongji_list = gongjiService.findAllSerachTitle(search, page-1);
			}
		}
		int Sblock = gongjiService.gongjiVO(page, searching, opt, search).getSblock();
		int Eblock = gongjiService.gongjiVO(page, searching, opt, search).getEblock();
		int totalpage = gongjiService.gongjiVO(page, searching, opt, search).getTotalpage();
		int Before = gongjiService.gongjiVO(page, searching, opt, search).getBefore();
		int Next = gongjiService.gongjiVO(page, searching, opt, search).getNext();
		if(Eblock > totalpage) {
			Eblock = totalpage;
		}
		model.addAttribute("searching", searching);
		model.addAttribute("gongji_list", gongji_list);
		model.addAttribute("pages",totalpage);
		model.addAttribute("page",page);
		model.addAttribute("opt", opt);
		model.addAttribute("search", search);
		model.addAttribute("Sblock",Sblock);
		model.addAttribute("Eblock",Eblock);
		model.addAttribute("Before",Before);
		model.addAttribute("Next",Next);
		
		return "gongji/findAll";
	}
}
