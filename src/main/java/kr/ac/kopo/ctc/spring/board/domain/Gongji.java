package kr.ac.kopo.ctc.spring.board.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity // DB 테이블 생성
public class Gongji {
   
   @Id //  Primary Key 설정
   @GeneratedValue(strategy=GenerationType.AUTO) // Auto Increment 의미
   @Column	// 테이블 컬럼 생성
   private Integer id; // 게시글 번호
   
   @Column	// 테이블 컬럼 생성
   private String title; // 게시글 제목
   
   @Column	// 테이블 컬럼 생성
   private String content; // 게시글 본문
   
   @Column	// 테이블 컬럼 생성
   private Date date; // 게시글 작성 날짜
   
   @Column	// 테이블 컬럼 생성
   private String author; // 게시글 작성자
   
   @Column	// 테이블 컬럼 생성
   private Integer viewcnt; // 조회수
      
   @ManyToOne(optional=false) // null 허용여부
   @JoinColumn(name="user_id") // FK
   private User user;
   
   // Goongji메소드 생성자
   public Gongji() {}
   
   //Gongji 메소드
   public Gongji(String title, String content, Date date, String author, int viewcnt) {
      this.title=title;
      this.content=content;
      this.date=date;
      this.author=author;
      this.viewcnt=viewcnt;
   }
   
   @Override
   public String toString() {
      String result = "[gongji_"+id+"]"+title+"["+author+"]";
      return result;
   }
   
   // 이하는 getter와 setter 생성
   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }
   public Date getDate() {
      return date;
   }
   public void setDate(Date date) {
      this.date = date;
   }
   public Integer getViewcnt() {
      return viewcnt;
   }
   public void setViewcnt(Integer viewcnt) {
      this.viewcnt = viewcnt;
   }

	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	      
}