package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.Sample;

@Repository // Repository는 저장소란 뜻을 가진 단어 (=DAO)
public interface SampleRepository extends JpaRepository<Sample, Long> {
	List<Sample> findAllByTitleLike(String title);
	List<Sample> findByIdGreaterThan(long id);
	
}


