package kr.ac.kopo.ctc.spring.board.service;

import java.util.List;

import kr.ac.kopo.ctc.spring.board.VO.GongjiVO;
import kr.ac.kopo.ctc.spring.board.domain.Gongji;

public interface GongjiService {
	// 페이징
	List<Gongji> findAll(int page);
	// 추가
	void create(String title, String content, String userid);
	// 삭제
	void delete(int id);
	// 수정
	Gongji update(String title, String content, int id);
	// 조회수 수정
	Gongji view(int id);
	// 작성자 검색
	List<Gongji> findAllSearchAutohr(String author, int page);
	// Read
	Gongji findByOne(int id);
	// 제목 검색
	List<Gongji> findAllSerachTitle(String title, int page);
	// 내용 검색
	List<Gongji> findAllSerachContent(String content, int page);
	// 전체 데이터 수
	public int findCount();
	//작성자 검색
	public int CountSearchAuthor(String author);
	// 제목 검색
	public int CountSearchTitle(String title);
	// 계정 추가
	void newUser(String userid, String passwd, String user, String email);
	// 아이디 중복확인
	public int IdCk(String Id);
	// 로그인 체크(세션용)
	public int loginCK(String Id, String passwd);
	//유저아이디 검색
	public String findById(String id);
	//페이징
	public GongjiVO gongjiVO(int page, boolean searching, String opt, String value);
}
