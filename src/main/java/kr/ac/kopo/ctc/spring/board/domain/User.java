package kr.ac.kopo.ctc.spring.board.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity // DB 테이블 생성
public class User {
	
   @Id	//  Primary Key 설정
   @GeneratedValue	// Auto Increment 의미
   @Column	// 테이블 컬럼 생성
   private Integer id;
   
   @Column	// 테이블 컬럼 생성
   //@NotEmpty(message="이름을 공백없이 2~6자로 입력해주세요.")
   // 패턴 설정
   @Pattern(regexp="\\S{4,10}", message="아이디를 공백없이 4~10자로 입력해주세요.")
   private String userid;
      
   @Column	// 테이블 컬럼 생성
   @Size(min=4, max=12, message="비밀번호를 4~12자로 입력해주세요.")	// 패턴 설정
   private String password;
   
   @Column	// 테이블 컬럼 생성
   // @NotEmpty(message="이름을 공백없이 2~6자로 입력해주세요.")
   //@Pattern(regexp="\\S{2,8}", message="이름을 공백없이 2~6자로 입력해주세요.")
   private String name;
   
   @Column	// 테이블 컬럼 생성
   @Email	// 이메일 패턴
   private String email;
      
   //아이디 중복확인 확인 위한 메서드
   public boolean isIdEqualToCheckId(String id, String checkId) {
       return id.equals(checkId);
   }
	
	// 일대다 매칭 설정
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user")
	private Collection<Gongji> gongjis;
	  
	public User() {}
	public User(String name) {
	      this.name=name;
	}

	// 이하는 getter와 setter 생성 
	public Integer getId() {
		return id;
	}
		
	public void setId(Integer id) {
		this.id = id;
	}
			
	public String getUserid() {
		return userid;
	}
		
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
		
	public String getName() {
	      return name;
	}

   public void setName(String name) {
      this.name = name;
   }

   public String getPassword() {
		return password;
	}
   
	public void setPassword(String password) {
		this.password = password;
	}
	
   public Collection<Gongji> getGongjis(){
      if(gongjis == null) {
         gongjis = new ArrayList<Gongji>();
      }
      return gongjis;
   }
   
   public void setGongjis(Collection<Gongji> gongjis) {
      this.gongjis = gongjis;
      
   }
   
   public void addGongji(Gongji g) {
      Collection<Gongji>gongjis = getGongjis();
      g.setUser(this);
      gongjis.add(g);
      
   }
   
   @Override
   public String toString() {
      String result = "["+id+"]"+name;
      for(Gongji g : getGongjis()) {
         result += "\n" + g.toString();
      }
      return result;
   }
}