<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>KSW01_Write</title>
</head>
<body>
<c:choose>
	<c:when test="${msg eq 'false'}">
		<script type="text/javascript">
			alert("미 가입자입니다.");
			window.location.href="sign";
	</script>
	</c:when>
	<c:otherwise>
       <script type="text/javascript">
			alert("등록되었습니다.");
			window.location.href="findAll";
	</script>
    </c:otherwise>
</c:choose>

</body>
</html>