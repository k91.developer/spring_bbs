<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 날짜 포맷을 위한 라이브러리 임포트 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<title>KSW01_게시물 수정</title>
<!-- 폼 유효성 체크 -->
<script type="text/javascript">
function test(){
	var obj = document.fr;

	if(obj.title.value == ''){
		alert("내용을 입력하세요"); 
		obj.title.focus();
		return false;
	}

	else if(obj.content.value == ''){
		alert("내용을 입력해 주세요"); 
		obj.content.focus();
		return false;
	}

	else if(confirm("수정 하시겠습니까?")){
        obj.submit();
    }
}
</script>
</head>
<body>
<center>
<H1>게시글 수정</H1>
<br>
<table border="1" cellspacing="0" width="80%">
<form action="write" name="fr">
<tr>
<th width="25%">번호</th><td width="25%">${gongji.id}</td>
<input type="hidden" name="id" value="${gongji.id}">
<th width="25%">날짜</th><td width="25%"><fmt:formatDate pattern="yyyy-MM-dd" value="${gongji.date}"/></td>
</tr><tr>
<th width="25%">작성자</th><td width="25%">${gongji.author}</td>
<th width="25%">조회수</th><td width="25%">${gongji.viewcnt}</td>
</tr><tr>
<th>제목</th><td colspan="3"><input type="text" name="title" value="${gongji.title}"></td>
</tr><tr>
<th>내용</th><td colspan="3"><textarea name="content" rows= "4" cols="10">${gongji.content}</textarea></td>
</tr>
</table>
<table>
<tr>
<td>
	<input type="button" value="목록" onclick="location='findAll'">
	<input type="button" value="수정"" onclick="test();">
	<input type="button" value="삭제" onclick="location='delete?id=${id}'">
</td>
</tr>
</form>
</table>
</center>
</body>
</html>