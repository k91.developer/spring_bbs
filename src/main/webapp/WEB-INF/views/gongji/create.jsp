<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 날짜 포맷을 위한 라이브러리 임포트 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_게시글 등록</title>
<!-- 폼 유효성 체크 -->
<script type="text/javascript">
function test(){
	var obj = document.fr;

	if(obj.author.value == ''){
		alert("아이디를 입력하세요"); 
		obj.author.focus();
		return false;
	}

	else if(obj.title.value == ''){
		alert("제목을 입력해 주세요"); 
		obj.title.focus();
		return false;
	}

	else if(obj.content.value == ''){
		alert("내용을 입력해 주세요"); 
		obj.content.focus();
		return false;
	}

	else if(confirm("등록 하시겠습니까?")){
        obj.submit();
    }
}
</script>
</head>
<body>
<center>
<%	// 세션 체크
String name = (String)session.getAttribute("sessionid");
%>
<H1>게시글 등록</H1>
<table border="1">
<form action = "write" name="fr">
<tr>

<%	// 세션이 있으면 해당 userid를 출력
	if(name!=null){
%>
	<th width="25%">아이디</th><td width="25%"><%=name%></td>
	<input type="hidden" name="author" value="<%=name%>">
<%
	}else{	// 세션 없으면 입력받는다.
%>
	<th width="25%">아이디</th><td width="25%"><input type="text" name="author"></td>
<%
	}
%>

<th width="25%">날짜</th><td width="25%"><fmt:formatDate pattern="yyyy-MM-dd" value="${date}"/></td>
</tr>
<tr>
<th>제목</th><td colspan="3"><input type="text" name="title"></td>
</tr>
<tr>
<th>내용</th><td colspan="3"><textarea rows="4" cols="50" name="content"></textarea></td>
</tr>
</table>
<table>
<tr>
<td colspan="2" id="end">
	<input type="button" value="등록" onclick="test();">
	<input type="button" value="목록" onclick="location='findAll'">
</td>
</tr>
</form>
</table>
</center>
</body>
</html>