<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_게시물 검색</title>
</head>
<body>
<center>
<H1>게시글 목록</H1>
<br>
<table border="1" cellspacing="0">
<tr>
<th width="10%">번호</th>
<th width="60%">제목</th>
<th width="10%">날짜</th>
<th width="10%">작성자</th>
<th width="10%">조회수</th>
</tr>
<c:forEach var="row" items="${gongji_list}">
<tr>
<td>${row.id}</td>
<td><a href="/SpringBoard/gongji/findOne?id=${row.id}&page=${page}">${row.title}</a></td>
<td>${row.date}</td>
<td>${row.user.name}</td>
<td>${row.viewcnt}</td>
</tr>
</c:forEach>
</table>
<table>
<tr>
<td colspan="5">
	<a id="page" href="/SpringBoard/gongji/search?page=1&opt=${opt}&search=${search}">&lt&lt</a>
	<a id="page" href="/SpringBoard/gongji/search?page=${Before}&opt=${opt}&search=${search}">&lt</a>
	<c:forEach var="i" begin="${Sblock}" end="${Eblock}">
	<c:choose>
		<c:when test="${page==i}">
			<a id="page" style="background-color:CadetBlue; color:white;" href="/SpringBoard/gongji/search?page=${i}&opt=${opt}&search=${search}">${i}</a>
		</c:when>
		<c:otherwise>
			<a id="page" href="/SpringBoard/gongji/search?page=${i}&opt=${opt}&search=${search}">${i}</a>
		</c:otherwise>
	</c:choose>	
	</c:forEach>
	<a id="page" href="/SpringBoard/gongji/search?page=${Next}&opt=${opt}&search=${search}">&gt</a>
	<a id="page" href="/SpringBoard/gongji/search?page=${pages}&opt=${opt}&search=${search}">&gt&gt</a>
</td>
</tr>
<tr>
<td id="end" width="20%"><input type="button" value="회원가입" onclick="location='sign'"></td>
<td>
<form action="search" method="GET">
<select name="opt">
	<option value="title">제목</option>
	<option value="author">아이디</option>
</select>
<input type="search" name="search" placeholder="검색어"><input type="submit" value="검색"></td>
</form>
<td id="end" width="20%"><input type="button" value="신규" onclick="location='create'"></td>
</tr>
</table>

</center>
</body>
</html>