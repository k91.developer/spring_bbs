<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<!-- CSS 임포트하기 -->
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_회원가입</title>
<script>
// 회원가입 유효성 체크
function test(){
	var obj = document.fr;

	if(obj.id.value == ''){
		alert("아이디를 입력하세요"); 
		obj.id.focus();
		return false;
	}
	else if(obj.id.value.length < 4 || obj.id.value.length > 10){
		alert("아이디는 4~10자 사이로 입력하세요"); 
		obj.id.focus();
		return false;
	}
	else if(obj.passwd.value == ''){
		alert("비밀번호를 입력해 주세요"); 
		obj.passwd.focus();
		return false;
	}
	else if(obj.passwd.value.length < 4 || obj.passwd.value.length > 12){
		alert("비밀번호를 4~12자로 입력해주세요."); 
		obj.passwd.focus();
		return false;
	}
	else if(obj.passwdCk.value == ''){
		alert("비밀번호를 입력해 주세요"); 
		obj.passwdCk.focus();
		return false;
	}
	else if(obj.passwd.value != obj.passwdCk.value){
		alert("비밀번호가 서로 다릅니다. 비밀번호를 확인해 주세요."); 
		obj.passwdCk.focus();
		return false;
	}
	else if(obj.username.value == ''){
		alert("이름을 입력해 주세요"); 
		obj.username.focus();
		return false;
	}
	else if(confirm("회원가입을 하시겠습니까?")){
        obj.submit();
    }
    obj.reset();
    document.getElementById("alert_text").innerHTML=('<span style="color:#777">아이디를 입력해주세요</span>')
    document.getElementById("alert_pw").innerHTML=('<span style="color:#777">비밀번호를 입력해주세요.</span>')
    document.getElementById("alert_pwd").innerHTML=('<span style="color:#777">비밀번호를 한번 더 입력해주세요.</span>')
}
//아이디 유효성 체크
function fun1(){
	var obj = document.fr;
	if(obj.id.value.length<4 || obj.id.value.lenth>10){
		document.getElementById("alert_text").innerHTML=('<span style="color:red;">아이디는 4~10자 사이로 입력해주세요</span>')
		return;		
	}else{
		document.getElementById("alert_text").innerHTML=('<span style="color:green;">정상적으로 입력되었습니다.</span>')
	}	
}
//비밀번호 유효성 체크
function fun2(){
	var obj = document.fr;
	if(obj.passwd.value== '' || obj.passwd.value != obj.passwdCk.value){
		document.getElementById("alert_pwd").innerHTML=('<span style="color:red;">비밀번호가 일치하지 않습니다.</span>')
		return;		
	}else{
		document.getElementById("alert_pwd").innerHTML=('<span style="color:green;">입력한 비밀번호와 일치합니다.</span>')
	}	
}
//2차비밀번호 유효성 체크
function fun3(){
	var obj = document.fr;
	if(obj.passwd.value== '' || obj.passwd.value.length < 4 || obj.passwd.value.length > 12){
		document.getElementById("alert_pw").innerHTML=('<span style="color:red;">비밀번호는 4~12자 사이로 입력해주세요.</span>')
		return;		
	}else{
		document.getElementById("alert_pw").innerHTML=('<span style="color:green;">정상적으로 입력되었습니다.</span>')
	}	
}
</script>
</head>
<body>
<center>
<H1>회원가입</H1>
<table>
<form action="signOK" method="POST" name="fr">
<tr>
	<th>아이디*</th>
	<td>
		<input type="text" name="id" onkeyup="fun1()">
		<span id="alert_text"><span style="color:#777">아이디를 입력해주세요</span></span>
	</td>
</tr>
<tr>
	<th>비밀번호*</th>
	<td><input type="password" name="passwd" onkeyup="fun3()">
	<span id="alert_pw"><span style="color:#777">비밀번호를 입력해주세요</span></span>
	</td>
</tr>
<tr>
	<th>비밀번호 확인*</th>
	<td>
	<input type="password" name="passwdCk" onkeyup="fun2()">
	<span id="alert_pwd"><span style="color:#777">비밀번호를 한번 더 입력해주세요</span></span>
	</td>
</tr>
<tr>
	<th>이름*</th>
	<td><input type="text" name="username"></td>
</tr>
<tr>
	<th>이메일</th>
	<td><input type="email" name="email"></td>
</tr>
</table>
<table>
<tr>
	<td id="end"><input type="button" value="가입" onclick="test();">
	<input type="button" value="목록" onclick="location='findAll'"></td>
</tr>
</form>
</table>
</center>
</body>
</html>