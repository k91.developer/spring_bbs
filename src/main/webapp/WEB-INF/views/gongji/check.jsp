<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 날짜 포맷을 위한 라이브러리 임포트 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<!-- CSS 임포트하기 -->
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_게시글 목록(Spring)</title>
</head>
<body>
<center>
<H1>게시글 목록</H1>
<%	// 세션 유무 확인
	if(session.getAttribute("sessionid")!=null){
%>	<!-- 세션이 있을 경우 세션 유저와 아이디로 출력 -->
	<H5>${sessionname}(${sessionid})님 환영합니다.</H5>
<%
	}
%>
<br>
<table border="1" cellspacing="0">
<tr>
	<th width="10%">번호</th>
	<th width="60%">제목</th>
	<th width="10%">날짜</th>
	<th width="10%">작성자</th>
	<th width="10%">조회수</th>
</tr>
<!-- JSPL for문 사용 -->
<c:forEach var="row" items="${gongji_list}">
<tr>
	<td>${row.id}</td>
	<td><a href="/SpringBoard/gongji/findOne?id=${row.id}&page=${page}">${row.title}</a></td>
	<td><fmt:formatDate pattern="yyyy-MM-dd" value="${row.date}"/></td>
	<td>${row.user.name}</td>
	<td>${row.viewcnt}</td>
</tr>
</c:forEach>
</table>
<table>
<tr>
<td colspan="5">
	<a id="page" href="/SpringBoard/gongji/check?page=1">&lt&lt</a>
	<a id="page" href="/SpringBoard/gongji/check?page=${Before}">&lt</a>
	<c:forEach var="i" begin="${Sblock}" end="${Eblock}">
	<c:choose>
		<c:when test="${page==i}">
			<a id="page" style="background-color:CadetBlue; color:white;" href="/SpringBoard/gongji/check?page=${i}">${i}</a>
		</c:when>
		<c:otherwise>
			<a id="page" href="/SpringBoard/gongji/check?page=${i}">${i}</a>
		</c:otherwise>
	</c:choose>	
	</c:forEach>
	<a id="page" href="/SpringBoard/gongji/check?page=${Next}">&gt</a>
	<a id="page" href="/SpringBoard/gongji/check?page=${pages}">&gt&gt</a>
</td>
</tr>
<tr>
<%	// 세션 유무에 따라 세션이 없으면 회원가입, 로그인 버튼 출력, 있으면 로그아웃 출력
	if(session.getAttribute("sessionid")==null){
%>
<td id="end" width="20%">
	<input type="button" value="회원가입" onclick="location='sign'">
	<input type="button" value="로그인" onclick="location='login'"></td>
<td>
<%
	}
	if(session.getAttribute("sessionid")!=null){
%>
	<td id="end" width="20%"><input type = "button" value="로그아웃" onclick="location='logout'"><td>
<%
	}
%>
<form action="search" method="GET">
<select name="opt">
	<option value="title">제목</option>
	<option value="author">아이디</option>
</select>
<input type="search" name="search" placeholder="검색어"><input type="submit" value="검색"></td>
</form>
<td id="end" width="20%"><input type="button" value="신규" onclick="location='create'"></td>
</tr>
</table>

</center>
</body>
</html>