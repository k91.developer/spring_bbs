<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<!-- CSS 임포트하기 -->
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_로그인</title>
<!-- 로그인 입력 폼 유효성 체크 -->
<script type="text/javascript">
function test(){
	var obj = document.fr;

	if(obj.id.value == ''){
		alert("아이디를 입력하세요"); 
		obj.id.focus();
		return false;
	}
	else if(obj.passwd.value == ''){
		alert("비밀번호를 입력해 주세요"); 
		obj.passwd.focus();
		return false;
	}
	obj.submit();
}
</script>
</head>
<body>
<center>
<H1>로그인</H1>
<table border="1" cellspacing="0">
<form action="loginOK" name="fr" method="POST">
<tr>
	<th><label for="id">아이디</label></th>
	<td><input type="text" id="id" name="id"></td>
</tr>
<tr>
	<th><label for="password">비밀번호</label></th>
	<td><input type="password" id="passwd" name="passwd"></td>
</tr>
</form>
</table>
<table cellspacing="0">
<tr>
<td>
<input type="button" value="취소" onclick="location='findAll'">
<input type="button" value="로그인" onclick="test();">
</td>
</tr>
</table>
</center>
</body>
</html>