<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<c:choose>
	<c:when test="${msg eq 'false'}">
		<script type="text/javascript">
			alert("아이디 중복체크를 하세요.");
			window.history.back();
	</script>
	</c:when>
	<c:when test="${msg eq 'true'}">
		<script type="text/javascript">
			alert("회원가입이 완료되었습니다.");
			window.location.href="findAll";
		</script>
	</c:when>
</c:choose>
</body>
</html>