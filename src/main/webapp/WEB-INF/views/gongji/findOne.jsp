<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 날짜 포맷을 위한 라이브러리 임포트 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<!-- CSS 임포트하기 -->
<link href="<c:url value="/mystyle.css" />" rel="stylesheet">
<meta charset="UTF-8">
<title>KSW01_게시물 보기(Spring))</title>
</head>
<body>
<center>
<H1>게시글 보기</H1>
<br>
<table border="1" cellspacing="0" width="80%">
<tr>
	<th width="25%">번호</th><td width="25%">${gongji.id}</td>
	<th width="25%">날짜</th><td width="25%"><fmt:formatDate pattern="yyyy-MM-dd" value="${gongji.date}"/></td>
</tr>
<tr>
	<th width="25%">작성자</th><td width="25%">${gongji.author}</td>
	<th width="25%">조회수</th><td width="25%">${gongji.viewcnt}</td>
</tr><tr>
	<th>제목</th><td colspan="3">${gongji.title}</td>
</tr><tr>
	<th>내용</th><td colspan="3">${gongji.content}</td>
</tr>
</table>
<table>
<tr>
<td colspan="4">
	<input type="button" value="목록" onclick="location='findAll?page=${page}'">
	<!-- gongji.author 값을 JSP에서 사용하기 위한 변수 선언 -->
	<c:set var="name" value="${gongji.author}" />
	<% // 세션 존재 확인
	if(session.getAttribute("sessionid")!=null){
		// JSP에서 사용하기 위해 선언했던 변수를 username에 할당
		String username = (String)pageContext.getAttribute("name");
		// 세션에 그 이름이 있을 경우를 판단 (있을 경우 수정버튼 출력)
		if(session.getAttribute("sessionid").equals(username)){
			%>
			<input type="button" value="수정" onclick="location='update?id=${id}'">
			<%
			}else{
				//out.println("세션있으나 계정이다름");
			}
		}else{
			//out.println("세션없음");
		}
	%>
</td>
</table>
</center>
</body>
</html>