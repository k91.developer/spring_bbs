<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
<c:choose>
	<c:when test="${msg eq 'true'}">
		<script type="text/javascript">
			alert("${sessionname}(${sessionid})님 환영합니다.");
			window.location.href="findAll";
	</script>
	</c:when>
	<c:when test="${msg eq 'false'}">
		<script type="text/javascript">
			alert("로그인 실패");
			window.location.href="login";
		</script>
	</c:when>
</c:choose>
<!--  세션확인용 -->
<%
	// Object obj1 = session.getAttribute("name");
	// String mySessionName = (String)obj1;
	// out.println(mySessionName);

%>

</body>
</html>